# -*- encoding: utf-8 -*-

import socket

gniazdo = socket.socket()

server_address = ('194.29.175.240', 31016)  # TODO: zmienić port!
serverListenerSocket = gniazdo.connect(server_address)

try:
    firstNumber = input("Podaj pierwsza liczbe: ")
    secondNumber = input("Podaj druga liczbe: ")

    try:
        float(firstNumber)
        float(secondNumber)
    except ValueError:
        print("Wpisz liczby.")

    gniazdo.send(bytes(firstNumber, 'UTF-8'))
    gniazdo.send(bytes(secondNumber, 'UTF-8'))

    odp = gniazdo.recv(1024)
    print(str(odp))

finally:
    gniazdo.close()
    pass