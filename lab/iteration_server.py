# -*- encoding: utf-8 -*-

import socket

# Ustawienie licznika na zero

counter = 0

# Tworzenie gniazda TCP/IP

gniazdo = socket.socket()

# Powiązanie gniazda z adresem
server_address = ('194.29.175.240', 31016)  # TODO: zmienić port!

# Nasłuchiwanie przychodzących połączeń
gniazdo.bind(server_address)
gniazdo.listen(1)


while True:
    # Czekanie na połączenie
    connection, client_address = gniazdo.accept()

    # Podbicie licznika
    counter += 1

    try:
        # Wysłanie wartości licznika do klienta
        connection.sendall(str(counter))
        pass

    finally:
        # Zamknięcie połączenia
        connection.close()
        pass
