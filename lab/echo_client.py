# -*- encoding: utf-8 -*-

import socket

# Tworzenie gniazda TCP/IP
gniazdo = socket.socket()

# Połączenie z gniazdem nasłuchującego serwera
server_address = ('194.29.175.240', 31016)  # TODO: zmienić port!
serverListenerSocket = gniazdo.connect(server_address)

try:
    # Wysłanie danych
    message = u'To jest wiadomość, która zostanie zwrócona.'
    gniazdo.sendall(message.encode('utf-8'))

    # Wypisanie odpowiedzi
    odp = gniazdo.recv(4096)
    print str(odp)

finally:
    # Zamknięcie połączenia
    gniazdo.close()
    pass